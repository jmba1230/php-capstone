<?php 
	require "../partials/template.php";

	function get_title(){
		echo "Profile";
	}

	function get_body_contents(){
		require "../controllers/connection.php";
 ?>

 <h1 class="text-center py-5">Profile Page</h1>

 <div class="container">
 	<div class="row">
 		<div class="col-lg-6">
 			 <?php
	      	if(isset($_SESSION['user'])&& $_SESSION['user']['role_id']==1){
	      	?>
	 
	      		<h1>Welcome! <?php echo $_SESSION['user']['firstName']?></h1>
	      
	      <?php 
	      	}else{
	      ?>
	      		<h3>User Details:</h3>
		      	<h4>Name: <?php echo $_SESSION['user']['firstName']." ".$_SESSION['user']['lastName']?></h4>
	      		<h4>Email: <?php echo $_SESSION['user']['email']?></h4>	

	      <?php 
	      	}
	      ?>

 		</div>
 		<div class="col-lg-6">
 			<h3>Addresses:</h3>
 			<ul>
 			<?php 

 				$user_id = $_SESSION['user']['id'];

 				$address_query = "SELECT * FROM addresses WHERE user_id=$user_id";

 				$addresses = mysqli_query($conn, $address_query);

 				foreach($addresses as $indiv_address){
 			?>

 			<li><?php echo $indiv_address['address1']. ", " . $indiv_address['address2']. "<br>" . $indiv_address['city']. " " . $indiv_address['zipcode'] ?></li>
 			<?php
	 			}
 			?>
 			</ul>
 			<form action="../controllers/add-address-process.php" method="POST" class="py-3">
 				<div class="form-group">
 					<label for="address1">Address 1:</label>
 					<input type="text" name="address1" class="form-control">
 				</div>
 				<div class="form-group">
 					<label for="address2">Address 2:</label>
 					<input type="text" name="address2" class="form-control">
 				</div>
 				<div class="form-group">
 					<label for="city">City:</label>
 					<input type="text" name="city" class="form-control">
 				</div>
 				<div class="form-group">
 					<label for="zipcode">Zip Code</label>
 					<input type="text" name="zipcode" class="form-control">
 				</div>
 				<input type="hidden" name="user_id" value="<?php echo $user_id?>">
 				<button class="btn btn-secondary" type="submit">Add Address</button>
 			</form>
 			<hr>


 			<h3>Contacts:</h3>
 			<ul>
 			<?php 

 				$user_id= $_SESSION['user']['id'];

 				$contact_query = "SELECT * FROM contacts WHERE user_id=$user_id";

 				$contacts = mysqli_query($conn, $contact_query);

 				foreach($contacts as $indiv_contact){
 			?>

 			<li><?php echo $indiv_contact['contactNO']?></li>
 			<?php
	 			}
 			?>
 			</ul>

 			<form action="../controllers/add-contact-process.php" method="POST" class="py-3">
 				<div class="form-group">
 					<label for="contacts">Contact Number:</label>
 					<input type="text" name="contacts" class="form-control">
 				</div>
 				<input type="hidden" name="user_id" value="<?php echo $user_id?>">
 				<button class="btn btn-secondary" type="submit">Add Contact</button>
 			</form>



 		</div>
 	</div>
 </div>


 <?php 
 	}
 ?>